class TwitterPullerController < ApplicationController
	skip_before_filter  :verify_authenticity_token
	before_filter :add_allow_credentials_headers

	def index
		hash_array = ['wobblykitty', 'wobblyrickroll', 'wobblytableflip', 
			'wobbly8bit']

		client = Twitter::REST::Client.new do |config|
  			config.consumer_key        = "JxOKSjkWbkg4cGUPa6D8hlBBe"
  			config.consumer_secret     = "0rdhyNDeIdWy8lEr9jwK8XAgQnbKJQSrwfaeG9oHvuqfKy9m4R"
  			config.access_token        = "3015839556-s8lZtiLRGshzUn7YN0qztaD9JIRbQzm5SEtYVsi"
  			config.access_token_secret = "REXAG68Vxe4i09iWCBGlbYL0CITdlb3e0imE3a4NqY8qX"
		end

		begin 
			result = client.search("#wobblycats OR #wobblyrickroll OR #wobblytableflip
			OR #wobbly8bit OR #wobblyallyourbase", result_type: "recent", count: 1).first
		rescue
			client = Twitter::REST::Client.new do |config|
  				config.consumer_key        = "oz9J6jWuszj186hnlDpUdtuLx"
  				config.consumer_secret     = "n9KDv2dXhUzFI2wVYNJZerTY3CTj2jGp3GcbpjrNNXXJrZdLgY"
  				config.access_token        = "3015839556-U3P5qtGVPoegndQvapAGH3s4zoAehyqCyAZF7T4"
  				config.access_token_secret = "9L7sr725GmKsfFum51Dn8nPQMpswo2nTouVR87dS8uq4Q"
			end
			result = client.search("#wobblycats OR #wobblyrickroll OR #wobblytableflip
			OR #wobbly8bit OR #wobblyallyourbase", result_type: "recent", count: 1).first
		end

		result_text = result.text
		result_id = result.id
		result_username = result.user.screen_name
		result_picture = result.user.profile_image_url
		result_hashtags = result.hashtags
		result_hashtag = ""

		result_hashtags.each do |item|
			if hash_array.include? item.text.downcase
				result_hashtag = item.text.downcase
				break
			end
		end
		Rails.logger.debug result
		render status: 200, json: { username: result_username,
			user_pic: result_picture.host + result_picture.path,
			message_id: result_id,
			message_text: result_text,
			message_hashtag: result_hashtag
		}
	end

	def add_allow_credentials_headers                                                                                                                                                                                                                                                        
  		# https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS#section_5                                                                                                                                                                                                      
  		#                                                                                                                                                                                                                                                                                       
  		# Because we want our front-end to send cookies to allow the API to be authenticated                                                                                                                                                                                                   
  		# (using 'withCredentials' in the XMLHttpRequest), we need to add some headers so                                                                                                                                                                                                      
  		# the browser will not reject the response                                                                                                                                                                                                                                             
  		response.headers['Access-Control-Allow-Origin'] = request.headers['Origin'] || '*'                                                                                                                                                                                                     
  		response.headers['Access-Control-Allow-Credentials'] = 'true'                                                                                                                                                                                                                          
	end 

	def options                                                                                                                                                                                                                                                                              
 	 head :status => 200, :'Access-Control-Allow-Headers' => 'accept, content-type'                                                                                                                                                                                                         
	end
end
